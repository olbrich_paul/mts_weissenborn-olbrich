﻿using System;

namespace taschenrechner_v0
{
    class Program
    {
        static void Main(string[] args)
        {
            Taschenrechner ts = new Taschenrechner();

            ConsoleKeyInfo swValue;


            // Display menu graphics
            Console.WriteLine("============================");
            Console.WriteLine("|   MENU SELECTION DEMO    |");
            Console.WriteLine("============================");
            Console.WriteLine("| Options:                 |");
            Console.WriteLine("|        1. Addieren       |");
            Console.WriteLine("|        2. Subtrahieren   |");
            Console.WriteLine("|        3. Dividieren     |");
            Console.WriteLine("|        4. Exit           |");
            Console.WriteLine("============================");
            Console.WriteLine(" Select option: ");

            swValue = Console.ReadKey();

            switch (swValue.Key)
            {
                case ConsoleKey.D1:
                    Console.WriteLine("4 + 5 = " + ts.add(4, 5));
                    break;

                case ConsoleKey.D2:
                    Console.WriteLine("4 - 5 = " + ts.sub(4, 5));
                    break;
                case ConsoleKey.D3:
                    Console.WriteLine("4 / 5 = " + ts.div(4, 5));
                    break;
                case ConsoleKey.D4:
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("Invalid selection");
                    break;
            }
        }
    }
}

